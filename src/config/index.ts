
export const DB:string = "mongodb://localhost:27017/nestposts"

export const REDIS_HOST:string = "127.0.0.1"

export const PORT:string | number = 3501