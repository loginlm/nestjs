
import {
  MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer, ConnectedSocket, OnGatewayDisconnect,
  OnGatewayConnection, WsResponse
} from '@nestjs/websockets'
import { InjectModel } from 'nestjs-typegoose';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Server } from 'socket.io';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { post } from 'src/db/model/post.model';





@WebSocketGateway()
export class EventsGateway {
  constructor(@InjectModel(post) private readonly postmodel: ModelType<post>) { }


  @WebSocketServer()
  server: Server;

  handleConnection(socket) {
    console.log('连接-----' + socket.id)
  }
  handleDisconnect(socket) {
    console.log('退出-----' + socket.id)
  }


  @SubscribeMessage('events')
  async findAll(@MessageBody() data: any) {
    let res = await this.postmodel.find()
    this.server.emit('list', res)
    return '123'
  }

  @SubscribeMessage('identity')
  async identity(@MessageBody() data, @ConnectedSocket() client,) {
    console.log(client.id)
    console.log(data)
    return data
  }




}