import { Module, Global } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DbModule } from 'src/db/db.module';
import { CommonService } from './common.service';

@Global()
@Module({
    imports: [
        DbModule,
        ConfigModule.forRoot({
            isGlobal:true
        })
    ],
    providers: [CommonService],
    exports: [DbModule],
})
export class CommonModule { }
