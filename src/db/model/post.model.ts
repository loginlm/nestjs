import { modelOptions, prop } from '@typegoose/typegoose';
import { ApiProperty } from '@nestjs/swagger'


@modelOptions({
    schemaOptions:{
        timestamps:true
    }
})
export class post {
    
    @ApiProperty({ description: '帖子标题', example: '帖子标题1' })
    @prop()
    title: string

    
    @ApiProperty({ description: '帖子内容', example: '帖子内容1' })
    @prop()
    content: string
}

