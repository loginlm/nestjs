
import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose'
import { post } from './model/post.model';
import { CacheService } from './cache/cache.service';

const models = TypegooseModule.forFeature([
    post
])


@Module({
    imports: [
        TypegooseModule.forRootAsync({
            useFactory: () => ({
                uri: process.env.DB,
                useCreateIndex: true,
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false
            })

        }),
        models
    ],
    exports: [models,CacheService],
    providers: [CacheService]
})

export class DbModule { }
