import { Module } from '@nestjs/common';
import { CommonModule } from './common/common.module';
import { AppController } from './modules/app/app.controller';
import { AppService } from './modules/app/app.service';
import { PostsModule } from './modules/posts/posts.module';
import { EventsGateway } from './gateway/events.gateway';
import { QueueModule } from './modules/queue/queue.module';



@Module({
  imports: [
    CommonModule,
    PostsModule,
    QueueModule
  ],
  controllers: [AppController],
  providers: [AppService, EventsGateway],
})
export class AppModule { }
