import { Controller, Get, Post, Put, Delete, Body, Param ,HttpService} from '@nestjs/common';
import { ApiTags, ApiOperation, ApiProperty } from '@nestjs/swagger';
//引入验证包
import { IsNotEmpty } from 'class-validator'
// 注入模型
import { InjectModel } from 'nestjs-typegoose';
import { post } from '../../db/model/post.model'

// 让postmodel有数据库操作提示
import { ModelType } from '@typegoose/typegoose/lib/types';
import { ReturnModelType } from '@typegoose/typegoose';
import { Crud } from 'nestjs-mongoose-crud'
import { map } from 'rxjs/operators';


//约束api输入数据  放到模型里面了
class createpost {
    
    @ApiProperty({ description: '帖子标题', example: '帖子标题1' })
    @IsNotEmpty({ message: '请填写帖子标题' })
    title: string

    @ApiProperty({ description: '帖子内容', example: '帖子内容1' })
    content: string
}

@Crud({
    model: post,
    routes: {
        find: {
            decorators: [ApiOperation({ summary: '博客帖子列表' })]
        },
        create: {
            dto: createpost
        }
    }
})

@Controller('posts')
@ApiTags('帖子')
export class PostsController {

    constructor(
        @InjectModel(post) private readonly model: ReturnModelType<typeof post>,
        private readonly httpserver:HttpService
    ) { }

    @Get('test')
    async test(){
        return await this.model.find({title:'test'})
    }


    @Get('http')
    async http(){
        return await this.httpserver.get('https://api.openweathermap.org/data/2.5/weather?q=cairo&appid=c9661625b3eb09eed099288fbfad560a').pipe(map(response => response.data))
    }
}
