import {
  InjectQueue, OnQueueActive, OnQueueCleaned, OnQueueCompleted, OnQueueDrained, OnQueueError, OnQueueFailed,
  OnQueuePaused, OnQueueProgress, OnQueueRemoved, OnQueueResumed, OnQueueStalled, OnQueueWaiting, Process, Processor,
} from '@nestjs/bull';
import { Logger, HttpService } from '@nestjs/common';
import { Job, Queue } from 'bull';
import { map } from 'rxjs/operators';
import { Observable, throwError, firstValueFrom } from 'rxjs'
@Processor('task')
export class QueueService {
  private readonly logger = new Logger(QueueService.name);
  constructor(
    @InjectQueue('task') private readonly taskQueue: Queue,
    private readonly httpserver: HttpService
  ) { }

  @Process('test')
  async processTask(job: Job) {
    /* this.logger.debug(`当前处的的任务ID-${job.id} VALUE-${job.data}`)
    await new Promise(resolve => {
      setTimeout(() => {
        resolve('');
      }, job.data * 1000);
    });
    this.logger.debug(`任务完成ID-${job.id}`) */
    await this.ceshi(job.data)
    return true
  }

  async ceshi(job) {
    console.log(job + '---' + 666)
    let num = this.getRandom(9000, 10000)
    await this.sleepWait(num)
    console.log(job + '---' + 777)
    // let source = this.httpserver.get('https://api.openweathermap.org/data/2.5/weather?q=cairo&appid=c9661625b3eb09eed099288fbfad560a').pipe(map(response => response.data))
    // let result = await firstValueFrom(source)
    // console.log(result)
  }


  getRandom(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min
  }


  sleepWait(time: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, time))
  }

  async cleanOldJobs() {
    /* (await this.taskQueue.getJobs(['completed'])).map(
      async job => await job.remove(),
    );
    (await this.taskQueue.getJobs(['failed'])).map(
      async job => await job.remove(),
    ); */
    await this.taskQueue.clean(1000)
    return { data: '队列清空完成' }
  }

  async showJobs() {

    //完成的任务
    //let completed = (await this.taskQueue.getJobs(['completed'])).map(job => job.id)
    let completed = (await this.taskQueue.getJobs(['completed']))

    //等待的任务
    let waiting = (await this.taskQueue.getJobs(['waiting']))

    //暂停的任务
    let paused = (await this.taskQueue.getJobs(['paused']))

    //失败的任务
    let failed = (await this.taskQueue.getJobs(['failed']))

    return { completed, waiting, paused, failed }
  }


  //handler(job: Job)-job任务已启动
  @OnQueueActive()
  onQueueActive(job: Job) {
    this.logger.debug(`任务已启动ID-${job.id}`)
  }


  //handler(error: Error) - 当错误发生时，error包括触发错误
  @OnQueueError()
  onQueueError(error: Error) {
    console.log('OnQueueError', error);
  }

  //排队要执行的任务
  @OnQueueWaiting()
  onQueueWaiting(jobId: number | string) {
    console.log('OnQueueWaiting', jobId);
  }

  //handler(job: Job)-job任务被标记为延迟。这在时间循环崩溃或暂停时进行调试工作时是很有效的
  @OnQueueStalled()
  onQueueStalled(job: Job) {
    console.log('OnQueueStalled', job.id);
  }

  //handler(job: Job, progress: number)-job任务进程被更新为progress值
  @OnQueueProgress()
  onQueueProgress(job: Job) {
    console.log('OnQueueProgress', job.id);
  }

  //handler(job: Job, result: any) job任务进程成功以result结束
  @OnQueueCompleted()
  onQueueCompleted(job: Job, result: any) {
    console.log('任务完成', job.id, result);
  }

  //handler(job: Job, err: Error)job任务以err原因失败
  @OnQueueFailed()
  onQueueFailed(job: Job, err: Error) {
    console.log('onQueueFailed', job.id, err);
  }

  //handler()队列被暂停
  @OnQueuePaused()
  onQueuePaused() {
    console.log('OnQueuePaused');
  }

  //handler(job: Job)队列被恢复
  @OnQueueResumed()
  onQueueResumed() {
    console.log('OnQueueResumed');
  }

  //handler(jobs: Job[], type: string) 旧任务从队列中被清理，job是一个被清理任务数组，type是要清理的任务类型
  @OnQueueCleaned()
  onQueueCleaned(jobs: Job[], type: string) {
    console.log('OnQueueCleaned', jobs, type);
  }

  //handler()在队列处理完所有等待的任务（除非有些尚未处理的任务被延迟）时发射出
  @OnQueueDrained()
  onQueueDrained() {
    console.log('当前队列已处理完毕');
  }

  //handler(job: Job)job任务被成功移除
  @OnQueueRemoved()
  onQueueRemoved(job: Job) {
    console.log('任务移除', job.id);
  }
}
