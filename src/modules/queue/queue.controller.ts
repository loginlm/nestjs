import { Controller, Get, Post, Delete, Put ,Param, Body } from '@nestjs/common';;
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { QueueService } from './queue.service';
import { ApiTags,ApiProperty } from '@nestjs/swagger';

class Task{
  @ApiProperty({ description: '名称', example: 'test' })
  name:string

  @ApiProperty({ description: '参数', example: 40 })
  num:number
}


@Controller('queue')
@ApiTags('队列')
export class QueueController {
  constructor(
    private readonly queueserice: QueueService,
    @InjectQueue('task') private readonly taskQueue: Queue,
  ) { }

  @Get()
  async getTasl() {
    await this.sleepWait(4000)
    return await this.queueserice.showJobs();
  }

  @Post(':id')
  async addtask(@Body() Body:Task) {
    
    return await this.taskQueue.add(Body.name, Body.num);
  }

  @Delete()
  async deleteOld() {
    return await this.queueserice.cleanOldJobs();
  }

  @Put()
  async toggle() {
    const status = await this.taskQueue.isPaused();
    if (status) {
      //恢复队列
      return await this.taskQueue.resume();
    }
    //暂停队列
    return await this.taskQueue.pause();
  }

  sleepWait(time: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, time))
  }
}
