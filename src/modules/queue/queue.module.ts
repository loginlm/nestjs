import { Module ,HttpModule} from '@nestjs/common';
import { QueueController } from './queue.controller';
import { QueueService } from './queue.service';
import { BullModule } from '@nestjs/bull';

@Module({
  imports:[
    BullModule.registerQueueAsync({
      name:'task',
      useFactory:()=>({
        redis:{
          host:process.env.REDIS_HOST,
          port: 6379,
          db: 2,
          password: ''
        }
      })
    }),
    HttpModule
  ],
  controllers: [QueueController],
  providers: [QueueService]
})
export class QueueModule {}
