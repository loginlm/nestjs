var message:string = "Hello World" 
console.log(message)
type laochen = string | number
type zhangsan = 1 | 3
let a:laochen = '123'

let b :zhangsan = 1


class p {
    constructor(public readonly a:string,public b:string){
        this.a = a;
        this.b = b;
    }
    sum():string{
      return this.a + this.b  
    }
}

let c = new p('123','456')
console.log(c.sum())

interface ads {
    a:string
    b:number
    c?:boolean
    d(s:number|string):number|string
}

interface UserArray{
    //定义数组结构
    [index:number]:string;
}
var arr:UserArray=['123', '22312'];
interface UserObj{
    //定义对象结构
    [index:string]:string;
}
var obj:UserObj={name:"2342"};


function star():number{
    return 123
}

star.call(obj)
star.apply(this,[])

abstract class Anmina{
    abstract eat():any
}

class Dog extends Anmina{
    eat(){
        return 'chi'
    }
}



interface Animal{
    eat():void;
}

interface Person extends Animal{
    work():void;
}

class Programmer{
    constructor(protected name:string){
        this.name = name
    }
    conde(code:string){
        console.log(this.name+ "  "+code)
    }
}
class Web extends Programmer implements Person,Animal{
    public name:string
    constructor(name:string){
        super(name)
        this.name = name
    }

    // eat必须定义
       eat(){
        console.log(this.name+"吃")
    }

    // work也必须定义
       work(){
        console.log(this.name+"工作")
    }
}


function getData<T>(value:T):T{
    return value;//传入什么返回什么
}
// 这样调用
getData<number>(123123);
getData<string>("12131");

let value = getData<number>(123456)

