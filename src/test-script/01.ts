function abc(a:string,b:string='123'):string[]{
    return ["123","123456"]
}

let aaa:string[] =  ["123","123456"]

interface po {
    p:string
    o:number
    q(c:string):string
}

let yy:po = {
    p:"123",
    o:23,
    q(e){
        return '123'
    }

}

console.log(yy.q("123"))