import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
// 引入api生成文档
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { Logger } from '@nestjs/common'
// import * as mongoose from 'mongoose';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const logger = new Logger()


  app.useGlobalPipes(new ValidationPipe())

  const options = new DocumentBuilder()
    .setTitle('api nestposts')
    .setDescription('第一个nest 博客项目')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-docs', app, document);

  const PORT = process.env.PORT || 3009
  await app.listen(PORT);
  //Logger.log(`http://localhost:${port}/api-docs`)

  console.log(`http://localhost:${PORT}/api-docs`)
}
bootstrap();
